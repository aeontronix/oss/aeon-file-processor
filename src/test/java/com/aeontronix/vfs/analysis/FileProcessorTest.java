package com.aeontronix.vfs.analysis;

import com.aeontronix.vfs.analysis.maven.MavenAnalysisReport;
import com.aeontronix.vfs.provider.zip.VFileSystemZipImpl;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

public class FileProcessorTest {
    @Test
    public void testAnalyse() throws Exception {
        try( VFileSystemZipImpl vfs = new VFileSystemZipImpl(new File(Objects.requireNonNull(getClass().getResource("/aeon-file-processor-1.0-SNAPSHOT.jar")).toURI())) ) {
            final FileAnalysisCombinedReport combinedReport;
            VFSAnalyser analyser = new VFSAnalyser();
            combinedReport = analyser.analyse(vfs);
            assertNotNull(combinedReport);
            final MavenAnalysisReport report = combinedReport.getReport(MavenAnalysisReport.class);
            assertNotNull(report);
            assertEquals("com.aeontronix.aeonfileprocessor", report.getGroupId());
            assertEquals("aeon-file-processor", report.getArtifactId());
            assertEquals("1.0-SNAPSHOT", report.getVersion());
            assertNull(report.getPackaging());
        }
    }
}
