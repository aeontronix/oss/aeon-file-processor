package com.aeontronix.vfs.provider.zip;

import com.aeontronix.vfs.AbsentVFile;
import com.aeontronix.vfs.VFile;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class VFileZipImpl implements VFile {
    private Map<String, VFileZipImpl> childrenIdx = new HashMap<>();
    private List<VFile> children = new ArrayList<>();
    private VFileSystemZipImpl vfs;
    private ZipArchiveEntry entry;
    private String name;

    public VFileZipImpl(VFileSystemZipImpl vfs, String name) {
        this(vfs, name, null);
    }

    public VFileZipImpl(VFileSystemZipImpl vfs, String name, ZipArchiveEntry entry) {
        this.vfs = vfs;
        this.entry = entry;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }


    @Override
    public String getPath() {
        return entry != null ? entry.getName() : "";
    }

    @Override
    public long getSize() {
        return entry.getSize();
    }

    @Override
    public boolean exists() {
        return true;
    }

    @Override
    public VFile getChild(String path) {
        String[] paths = path.split("/", 2);
        VFileZipImpl child = childrenIdx.get(paths[0]);
        if (child == null) {
            return new AbsentVFile(path);
        } else if (paths.length > 1) {
            return child.getChild(paths[1]);
        } else {
            return child;
        }
    }

    @Override
    public List<VFile> listFiles() {
        return Collections.unmodifiableList(children);
    }

    @Override
    public boolean isFile() {
        return entry != null && !entry.isDirectory();
    }

    @Override
    public boolean isDirectory() {
        return entry == null || entry.isDirectory();
    }

    @Override
    public InputStream getInputStream() throws IOException {
        if( entry != null ) {
            return vfs.getZipFile().getInputStream(entry);
        } else {
            return null;
        }
    }

    public void init(String name, ZipArchiveEntry entry) {
        String[] paths = name.split("/", 2);
        if (paths.length > 1) {
            String childName = paths[0];
            VFileZipImpl child = childrenIdx.get(childName);
            if (child == null) {
                child = new VFileZipImpl(vfs, childName);
                addChild(childName, child);
            }
            child.init(paths[1], entry);
        } else {
            addChild(name, new VFileZipImpl(vfs, name, entry));
        }
    }

    private void addChild(String name, VFileZipImpl child) {
        childrenIdx.put(name, child);
        children.add(child);
    }
}
