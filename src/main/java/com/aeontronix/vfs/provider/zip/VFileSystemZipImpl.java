package com.aeontronix.vfs.provider.zip;

import com.aeontronix.vfs.VFile;
import com.aeontronix.vfs.VFileSystem;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

public class VFileSystemZipImpl extends VFileSystem {
    private final ZipFile zipFile;
    private VFileZipImpl root;

    public VFileSystemZipImpl(File zipFile) throws IOException {
        this(new ZipFile(zipFile));
    }

    public VFileSystemZipImpl(ZipFile zipFile) throws IOException {
        this.zipFile = zipFile;
        this.root = new VFileZipImpl(this, "/");
        Enumeration<ZipArchiveEntry> e = zipFile.getEntries();
        while (e.hasMoreElements()) {
            ZipArchiveEntry entry = e.nextElement();
            String name = entry.getName();
            name = stripSlashes(name);
            root.init(name, entry);
        }
    }

    ZipFile getZipFile() {
        return zipFile;
    }

    @Override
    public void close() throws Exception {
        zipFile.close();
    }

    @Override
    public VFile getFile(String path) {
        return root.getChild(stripSlashes(path));
    }

    @Override
    public List<VFile> listFiles() {
        return Collections.list(zipFile.getEntries()).stream().map(e -> new VFileZipImpl(this, e.getName(), e))
                .collect(Collectors.toList());
    }

    static String stripSlashes(String name) {
        if (name.startsWith("/")) {
            name = name.substring(1);
        }
        if (name.endsWith("/")) {
            name = name.substring(0, name.length() - 1);
        }
        return name;
    }
}
