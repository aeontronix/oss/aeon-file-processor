package com.aeontronix.vfs;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface VFile {
    String getName();

    String getPath();

    long getSize();

    boolean exists();

    VFile getChild(String path);

    List<VFile> listFiles();

    boolean isFile();

    boolean isDirectory();

    InputStream getInputStream() throws IOException;
}
