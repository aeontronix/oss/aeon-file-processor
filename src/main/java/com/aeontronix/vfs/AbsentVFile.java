package com.aeontronix.vfs;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

public class AbsentVFile implements VFile {
    private String name;

    public AbsentVFile() {
        this.name = "";
    }

    public AbsentVFile(String name) {
        this.name = name;
    }

    @Override
    public long getSize() {
        return -1;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getPath() {
        return name;
    }

    @Override
    public boolean exists() {
        return false;
    }

    @Override
    public List<VFile> listFiles() {
        return Collections.emptyList();
    }

    @Override
    public boolean isFile() {
        return false;
    }

    @Override
    public boolean isDirectory() {
        return false;
    }

    @Override
    public InputStream getInputStream() {
        return null;
    }

    @Override
    public VFile getChild(String path) {
        return new AbsentVFile(path);
    }
}
