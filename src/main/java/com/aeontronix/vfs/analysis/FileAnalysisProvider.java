package com.aeontronix.vfs.analysis;

import com.aeontronix.vfs.VFileSystem;

public interface FileAnalysisProvider {
    FileAnalysisReport analyse(VFileSystem vfs) throws Exception;
}
