package com.aeontronix.vfs.analysis;

import com.aeontronix.commons.exception.UnexpectedException;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class FileAnalysisError {
    private String message;
    private String stackTrace;

    public FileAnalysisError(String message, String stackTrace) {
        this.message = message;
        this.stackTrace = stackTrace;
    }

    public FileAnalysisError(Throwable t) {
        this.message = t.getMessage();
        try (StringWriter tmp = new StringWriter(); final PrintWriter w = new PrintWriter(tmp)) {
            t.printStackTrace(w);
            this.stackTrace = tmp.toString();
        } catch (IOException e) {
            throw new UnexpectedException(e);
        }
    }

    public String getMessage() {
        return message;
    }

    public String getStackTrace() {
        return stackTrace;
    }
}
