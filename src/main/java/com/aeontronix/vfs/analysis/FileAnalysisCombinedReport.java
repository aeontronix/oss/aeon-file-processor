package com.aeontronix.vfs.analysis;

import java.util.HashMap;
import java.util.Map;

public class FileAnalysisCombinedReport {
    private Map<String, FileAnalysisReport> reports = new HashMap<>();

    public Map<String, FileAnalysisReport> getReports() {
        return reports;
    }

    public void addReport(FileAnalysisReport report) {
        addReport(report.getClass().getName(), report);
    }

    public void addReport(String name, FileAnalysisReport report) {
        reports.put(name, report);
    }

    public <X extends FileAnalysisReport> X getReport(Class<X> analyserClass) {
        return analyserClass.cast(reports.get(analyserClass.getName()));
    }

    public FileAnalysisReport getReport(String name) {
        return reports.get(name);
    }
}
