package com.aeontronix.vfs.analysis;

import java.util.ArrayList;

public class FileAnalysisReport {
    private ArrayList<FileAnalysisError> errors = new ArrayList<>();
    private ArrayList<String> warnings = new ArrayList<>();

    public FileAnalysisReport() {
    }

    public FileAnalysisReport(Throwable t) {
        addError(t);
    }

    public void addError(String message) {
        errors.add(new FileAnalysisError(message, null));
    }

    public void addError(Throwable t) {
        errors.add(new FileAnalysisError(t));
    }

    public void addWarning(String warning) {
        warnings.add(warning);
    }

    public ArrayList<FileAnalysisError> getErrors() {
        return errors;
    }

    public void setErrors(ArrayList<FileAnalysisError> errors) {
        this.errors = errors;
    }

    public ArrayList<String> getWarnings() {
        return warnings;
    }

    public void setWarnings(ArrayList<String> warnings) {
        this.warnings = warnings;
    }
}
