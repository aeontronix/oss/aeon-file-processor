package com.aeontronix.vfs.analysis;

import com.aeontronix.vfs.VFileSystem;
import com.aeontronix.vfs.analysis.maven.MavenFileAnalysisProvider;

import java.io.*;
import java.util.Arrays;
import java.util.HashSet;

public class VFSAnalyser {
    private HashSet<FileAnalysisProvider> providers = new HashSet<>();

    public VFSAnalyser() throws IOException {
        this(new MavenFileAnalysisProvider());
    }

    public VFSAnalyser(FileAnalysisProvider... providers) {
        if (providers != null) {
            this.providers.addAll(Arrays.asList(providers));
        }
    }

    public FileAnalysisCombinedReport analyse(VFileSystem vfs) throws Exception {
        final FileAnalysisCombinedReport combinedReport = new FileAnalysisCombinedReport();
        for (FileAnalysisProvider analyzer : providers) {
            combinedReport.addReport(analyzer.analyse(vfs));
        }
        return combinedReport;
    }
}
