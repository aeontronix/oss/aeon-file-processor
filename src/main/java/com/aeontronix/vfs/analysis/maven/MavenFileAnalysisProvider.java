package com.aeontronix.vfs.analysis.maven;

import com.aeontronix.commons.StringUtils;
import com.aeontronix.commons.xml.XPathUtils;
import com.aeontronix.commons.xml.XmlUtils;
import com.aeontronix.vfs.AbsentVFile;
import com.aeontronix.vfs.VFile;
import com.aeontronix.vfs.VFileSystem;
import com.aeontronix.vfs.analysis.FileAnalysisProvider;
import com.aeontronix.vfs.analysis.FileAnalysisReport;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MavenFileAnalysisProvider implements FileAnalysisProvider {
    @Override
    public FileAnalysisReport analyse(VFileSystem vfs) throws Exception {
        return analyse(new MavenAnalysisReport(), vfs);
    }

    protected FileAnalysisReport analyse(MavenAnalysisReport report, VFileSystem vfs) throws Exception {
        final VFile pomXml = findPom(vfs, report);
        if (pomXml.exists()) {
            if (pomXml.isFile()) {
                try (InputStream is = pomXml.getInputStream()) {
                    analysePomFile(report, is);
                }
            }
        }
        return report;
    }

    public static void analysePomFile(MavenAnalysisReport report, InputStream is) throws IOException, SAXException, XPathExpressionException {
        final Document pomDoc = XmlUtils.parse(is, false);
        report.setPomFound(true);
        report.setPomXml(pomDoc);
        final String artifactId = XPathUtils.evalXPathString("/project/artifactId", pomDoc);
        final String packaging = XPathUtils.evalXPathString("/project/packaging", pomDoc);
        String groupId = XPathUtils.evalXPathString("/project/groupId", pomDoc);
        if (StringUtils.isBlank(groupId)) {
            groupId = XPathUtils.evalXPathString("/project/parent/groupId", pomDoc);
        }
        String version = XPathUtils.evalXPathString("/project/version", pomDoc);
        if (StringUtils.isBlank(version)) {
            version = XPathUtils.evalXPathString("/project/parent/version", pomDoc);
        }
        report.setPackaging(packaging);
        report.setArtifactId(artifactId);
        report.setGroupId(groupId);
        report.setVersion(version);
        report.setName(XPathUtils.evalXPathString("/project/name", pomDoc));
    }

    protected VFile findPom(VFileSystem vfs, MavenAnalysisReport report) {
        VFile mavenFolder = vfs.getFile("META-INF/maven");
        if (mavenFolder.exists()) {
            VFile mavenGroupFolder = findSingleChildFolder(mavenFolder, report);
            if (mavenGroupFolder.exists()) {
                VFile mavenArtifactFolder = findSingleChildFolder(mavenGroupFolder, report);
                if (mavenArtifactFolder.exists()) {
                    VFile pomXml = mavenArtifactFolder.getChild("pom.xml");
                    if (pomXml.exists()) {
                        return pomXml;
                    }
                }
            }
        }
        return new AbsentVFile("");
    }

    private VFile findSingleChildFolder(VFile maven, MavenAnalysisReport report) {
        List<VFile> childrens = maven.listFiles();
        int count = childrens.size();
        if (count == 0) {
            return new AbsentVFile("");
        } else if (count > 1) {
            report.addError("Multiple maven folders found");
            return new AbsentVFile("");
        } else {
            return childrens.get(0);
        }
    }
}
