package com.aeontronix.vfs.analysis.maven;

import com.aeontronix.vfs.analysis.FileAnalysisReport;
import org.w3c.dom.Document;

public class MavenAnalysisReport extends FileAnalysisReport {
    private boolean pomFound;
    private String name;
    private String groupId;
    private String artifactId;
    private String version;
    private String packaging;
    private Document pomXml;

    public boolean isPomFound() {
        return pomFound;
    }

    public void setPomFound(boolean pomFound) {
        this.pomFound = pomFound;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getPackaging() {
        return packaging;
    }

    public void setPackaging(String packaging) {
        this.packaging = packaging;
    }

    public Document getPomXml() {
        return pomXml;
    }

    public void setPomXml(Document pomXml) {
        this.pomXml = pomXml;
    }
}
