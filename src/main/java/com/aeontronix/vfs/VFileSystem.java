package com.aeontronix.vfs;

import com.aeontronix.vfs.provider.zip.VFileSystemZipImpl;
import com.aeontronix.vfs.provider.zip.VFileZipImpl;

import java.io.File;
import java.io.IOException;
import java.util.List;

public abstract class VFileSystem implements AutoCloseable {
    public abstract VFile getFile(String path);

    public static VFileSystem create(File file) throws IOException {
        String path = file.getPath();
        if(path.endsWith(".zip") || path.endsWith(".jar") ) {
            return new VFileSystemZipImpl(file);
        } else throw new IllegalArgumentException("Unsupported VFileSystem file: "+path);
    }

    public abstract List<VFile> listFiles();
}
